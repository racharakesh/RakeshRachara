package com.insurance.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.insurance.models.CustomerDetails;
import com.insurance.validation.PremiumValidations;

public class SampleTest {
	@Before
	public void setUp() throws Exception {
		CustomerDetails details = new CustomerDetails();
		details.setAge(33);
		details.setGender("M");
		details.setHabits(null);
		details.setHealth(null);
		details.setName("rakesh");

    }
	
	@Test
	public void estimateTotalPremiumAmount(CustomerDetails customerDetails) {
		PremiumValidations premiumValidations = new PremiumValidations();
		Assert.assertEquals(1234, premiumValidations.getPremiumAmount(customerDetails));
	}
}
