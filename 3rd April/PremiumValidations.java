package com.insurance.validation;

import com.insurance.models.CustomerDetails;
import com.insurance.models.HabitsValidator;
import com.insurance.models.HealthValidator;

public class PremiumValidations extends PremiumCalculatorStrategyImpl{
	
	public PremiumValidations(){
		
	}

	PremiumValidations premiumValidations = null;

	private int getPremiumByAge(Integer age) {
		return premiumValidations.getPremiumAmount(age);
	}

	private int getPremiumByHabbitsValidator(HabitsValidator habitsValidator) {
		return premiumValidations.getPremiumAmount(habitsValidator);
	}

	private int getPremiumByHealthValidator(HealthValidator healthValidator) {
		return premiumValidations.getPremiumAmount(healthValidator);
	}

	public int estimateTotalPremiumAmount(CustomerDetails customerDetails) {

		premiumValidations = new PremiumValidations();
		
		int estimatedAmount = baseMinAmount + 
				getPremiumByAge(customerDetails.getAge())
				+ getPremiumByHabbitsValidator(customerDetails.getHabits())
				+ getPremiumByHealthValidator(customerDetails.getHealth()); 

		return estimatedAmount;
	}

	
}
