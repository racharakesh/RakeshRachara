package com.insurance.projection;

import java.util.Scanner;

import com.insurance.models.HealthValidator;
import com.insurance.models.HabitsValidator;
import com.insurance.models.CustomerDetails;
import com.insurance.validation.PremiumValidations;

public class ProjectPremium {

	public static void main(String[] args) {

		CustomerDetails customerDetails = new CustomerDetails();
		HealthValidator healthValidator = new HealthValidator();
		HabitsValidator habitsValidator = new HabitsValidator();

		PremiumValidations calculator = new PremiumValidations();
		try {
			Scanner sc = new Scanner(System.in);
			System.out.println("enter name ");
			customerDetails.setName(sc.next());
			System.out.println("enter age ");
			customerDetails.setAge(sc.nextInt());
			System.out.println("enter Gender ");
			customerDetails.setGender(sc.next());
			System.out.println("Enter true/fase for the following questions ... ");
			System.out.println("is there Blood Pressure? ");
			healthValidator.setBloodPressure(sc.nextBoolean());
			System.out.println("is there Bool Sugar? ");
			healthValidator.setBloodSugar(sc.nextBoolean());
			System.out.println("is there Hyber Tension? ");
			healthValidator.setHyperTension(sc.nextBoolean());
			System.out.println("Are you overweoght? ");
			healthValidator.setOverWeight(sc.nextBoolean());

			System.out.println("Are you Alcoholic? ");
			habitsValidator.setAlcohol(sc.nextBoolean());
			System.out.println("Do you consue Drugs? ");
			habitsValidator.setDrugs(sc.nextBoolean());
			System.out.println("Do you do Excercise? ");
			habitsValidator.setExercise(sc.nextBoolean());
			System.out.println("Are you Smoker? ");
			habitsValidator.setSmoking(sc.nextBoolean());
		} catch (Exception e) {
			e.printStackTrace();
		}
		customerDetails.setHealth(healthValidator);
		customerDetails.setHabits(habitsValidator);

		int premium = calculator.estimateTotalPremiumAmount(customerDetails);

		System.out.println("The calculated premimum for the customer = "
				+ premium);

	}

}
