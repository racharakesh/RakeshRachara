package com.insurance.strategies;

public interface PremiumCalculatorStrategy {
	
	public static final Integer baseMinAmount = 5000;
	public int getPremiumAmount(Object object);
	
}
